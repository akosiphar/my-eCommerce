import React, { useEffect, useState } from 'react'
import { Badge, Card, Container, Table } from 'react-bootstrap';

export default function UserOrders() {
    const [myOrder, setMyOrder] = useState([])
    const [isDelivered, setIsDelivered] = useState(Boolean)
    console.log(isDelivered)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/userOrders`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data.isDelivered);
        setMyOrder(data)
        setIsDelivered(data.isDelivered)
        
    })
    }, [isDelivered])

    const randomColor = () => {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);
        return `rgb(${r}, ${g}, ${b})`;
    };
    
    const getContrastColor = (color) => {
        // convert color to RGB values
        const rgb = color.substring(4, color.length - 1)
            .replace(/ /g, '')
            .split(',');
        const r = parseInt(rgb[0]);
        const g = parseInt(rgb[1]);
        const b = parseInt(rgb[2]);
    
        // calculate color brightness
        const brightness = (r * 250 + g * 500 + b * 200) / 1000;
    
        // return black or white based on brightness
        return brightness > 128 ? 'black' : 'white';
    };



  return (
    <>
    <Container>
    {
            myOrder.map((user, index) =>
                <Card key={index} className="my-3" >
                <Card.Header style={{backgroundColor: randomColor(), color: getContrastColor(randomColor())}}>
                    <p className='h4'>Order Id: {user._id}</p>
                </Card.Header>
                <Card.Header className='bg-dark text-light' key={index} >
                    {/* <p className='h4'>{user.products[0].productName}</p> */}
                </Card.Header>
                    <Card.Body>
                    <Container fluid className='d-flex'>
                        <Container fluid>
                        <Card.Subtitle>Date Purchased</Card.Subtitle>
                        <Card.Text>{user.purchasedOn}</Card.Text>
                        <Card.Subtitle>Total Amount:</Card.Subtitle>
                        <Card.Text>{user.totalPrice}</Card.Text>
                        </Container>   
                        <Container>
                            {user.isDelivered ?
                                <p className='h5'>
                                <Badge bg='success' className='p-2'>Delivered</Badge>
                                </p>
                                :
                                <p className='h5'>
                                <Badge bg='danger' className='p-2'>Pending</Badge>
                                </p>
                            }
                        </Container>   
                    </Container>
                        <Container className='p-2 w-50 d-flex flex-column' >
                        {
                            user.orders.map((prod, num) =>
                                <Table fluid="true" key={num} striped bordered hover variant='info'>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{prod.productName}</td>
                                            <td>{prod.quantity}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            )
                        }
                        </Container>
                    </Card.Body>
                </Card>
            )
        }
    </Container>


    </>
  )
}
