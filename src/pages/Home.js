import React, { useEffect } from 'react'
import { Button, Carousel, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'
import  home1 from '../images/image1.jpg'
import  home2 from '../images/sari-sari_1.jpg'
import  home3 from '../images/sari-sari_2.jpg'
import  home4 from '../images/sari-sari_3.jpg'
import './Home.css'

export default function Home() {

  useEffect(() => {
    document.title = "Home"
})
  return (
    <div className='position-relative'>
    <motion.div
      initial={{translateY: 100}}
      animate={{translateY: 0, transition: {duration: .3}}}
    >
      <Container fluid className='position-absolute' style={{zIndex: 10, top: "25%", left: "5%"}}>
        <Container fluid className='col-12 col-s-8 col-lg-4 row align-content-center align-items-center text-white h-100'>
          <h1 className='bg-dark title'>Welcome To My Sari-Sari Store</h1>
          <p className='lead bg-dark'>You Can Find Anything in this Store</p>
          <Button className='col col-auto' variant='dark' as={Link} to="/products">See All products</Button>
        </Container>
      </Container>

      <Container fluid className='g-0' >
      <Carousel fade controls={false} interval={3000} pause={false}>
      <Carousel.Item>
        <img
          className="d-block w-100 image-fluid"
          style={{backgroundSize: "cover", height: "90vh"}}
          src={home2}
          alt="First slide"
        />
        {/* <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption> */}
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          style={{backgroundSize: "cover", height: "90vh"}}
          src={home3}
          alt="Second slide"
        />

        {/* <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption> */}
      </Carousel.Item>
    </Carousel>
      </Container>

    </motion.div>
    </div>
  )
}
