import React, { useContext, useEffect, useState } from 'react'
import { Button, Card, Container, Image, Modal, Table } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom';
import ProductsArray from '../components/ProductsArray'
import profile_avatar from '../images/profile_avatar.png'
import { motion } from 'framer-motion';
import { UserContext } from '../UserContext';

export default function UserProfile() {
    const [myCart, setMyCart] = useState([])
    const [myProfile, setMyProfile] = useState([])
    const {user} = useContext(UserContext)


    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true)
    
    
    
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setMyCart(data.orderedProduct)
            setMyProfile(data)
            // user.badge(data.orderedProduct.length)
            
        })
        
        document.title = `${myProfile.firstName}'s profile`

    }, [myProfile.firstName, myProfile.orderedProduct]) // to instant update the state
    

  return (
    <>
    {/* User Card */}
    <Container>
        <Card className='my-3 p-3 d-flex flex-md-row flex-column justify-content-md-evenly align-items-center'>
                    <Image src={profile_avatar} fluid={true} className='w-25'/>
                {/* <Container className=''>
                </Container> */}
                <div>
                    <Card.Body>
                    <Card.Title>Name:</Card.Title>
                    <Card.Text>{myProfile.firstName} {myProfile.lastName}</Card.Text>
                    <Card.Title>Email:</Card.Title>
                    <Card.Text>{myProfile.email}</Card.Text>
                    </Card.Body>
                </div>
        </Card>
    </Container>
    <Container fluid className='d-flex justify-content-end my-3'>
        <Button onClick={handleShow}>Checkout</Button>
    </Container>
        

    {/* Cart Card */}
    <Card>
        <Card.Header className='bg-dark text-white p-4'>
        <p className='h3'>My Cart</p>
            <Container fluid className='d-flex flex-wrap justify-content-xl-start justify-content-center gap-2 text-dark'>
                {
                    myCart.map((user, index, i) =>
                    <motion.div key={user._id}
                            whileHover={{
                                scale: 1.1,
                                // transition:{ type: 'spring', stiffness:400, damping:10}
                                }}
                            initial={{opacity: 0, translateX: 100}}
                            animate={{opacity: 1, translateX: 0, 
                                transition:{duration: 0.3, delay: i * 0.1}
                            }}
                            >
                        <Card key={index} className="my-3">
                        <Card.Header className='bg-dark text-light' >
                            <p className='h4'>{user.products[0].productName}</p>
                        </Card.Header>
                            <Card.Body>
                                <Card.Subtitle>Date Added</Card.Subtitle>
                                <Card.Text>{user.purchasedOn}</Card.Text>
                                <Card.Subtitle>Total Amount:</Card.Subtitle>
                                <Card.Text>{user.totalAmount}</Card.Text>
                                {
                                    user.products.map((prod, num) =>
                                    <ProductsArray key={prod.productId} productProps={prod} />
                                        
                                    )
                                }
                            </Card.Body>
                        </Card>
                    </motion.div>
                    )
                }
            </Container>
        </Card.Header>
    </Card>
    
    

    <Modal show={show} onHide={handleClose} scrollable={true}>
        <Modal.Header closeButton className='bg-dark'>
            <Modal.Title>
            <p className='h3 text-white'>Proceed to Checkout?</p>
            </Modal.Title>
        </Modal.Header>
            <Modal.Body>
            <Container fluid className='d-flex flex-wrap justify-content-xl-start justify-content-center gap-2'>
        {
            myCart.map((user, index) =>
                <Card key={index} className="">
                <Card.Header className='bg-dark text-light'>
                    <p className='h4'>{user.products[0].productName}</p>
                </Card.Header>
                    <Card.Body>
                        <Card.Subtitle>Date of Purchase</Card.Subtitle>
                        <Card.Text>{user.purchasedOn}</Card.Text>
                        <Card.Subtitle>Total Amount:</Card.Subtitle>
                        <Card.Text>{user.totalAmount}</Card.Text>
                        {
                            user.products.map((prod, num) =>
                                <div key={prod}>
                                    <Card.Subtitle>Quantity</Card.Subtitle>
                                    <Card.Text>{prod.quantity}</Card.Text>
                                </div>,
                            {/* <ProductsArray key={prod.productId} productProps={prod} /> */}
                                
                            )
                        }
                    </Card.Body>
                </Card>
            )
        }
            </Container>
        </Modal.Body>
            <Button as={Link} to={"/userProfile/checkout"} type="submit" variant='primary' onClick={handleClose}>Proceed to Checkout</Button>
        </Modal>
    </>
  )
}
