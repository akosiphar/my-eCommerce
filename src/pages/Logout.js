import React, { useContext, useEffect } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import { UserContext } from '../UserContext';

export default function Logout() {
    const {unSetUser, setUser} = useContext(UserContext)
    const navigate = useNavigate()

    Swal.fire({
        title: "You have been logout",
        icon: "warning",
        timer: 2000
    })

    
    unSetUser();
    
    useEffect(() => {
        setUser({
            email: null,
            id: null
        })
    })

    return (
        <Navigate to="/login" />
    )
}
