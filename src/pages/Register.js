import React, { useContext, useEffect, useState } from 'react'
import { Button, Card, Container, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import  Swal from 'sweetalert2'
import { motion } from 'framer-motion'

export default function Register() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [mobileNo, setMobileNumber] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const navigate = useNavigate()
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        document.title = "Register"
    })

    const handleRegister = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data) {

                Swal.fire({
                    title: "Registered Successfully",
                    icon: "success",
                    text: "Proceed to Login"
                })
                
                navigate("/login")

            } else {

                Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Missing Details/Email already exists"
                })
            }
        })
    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2])


  return (
    <motion.div
        initial={{rotateY: 180, display: "none"}}
        animate={{rotateY: 360, display: "block", transition: {duration: 0.5}}}
        exit={{rotateY: 180, display: "none"}}
    >
    <Container className='col-md-5'>
    <Card className='mx-auto mt-5'>
    <Card.Header className='bg-dark text-light text-center'>
        <h1>Register</h1>
    </Card.Header>
        <Form onSubmit={handleRegister} className="p-3">
            <Form.Group controlId="userFirstName" className='mb-3'>
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type='text'
                    placeholder='First Name'
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    />
            </Form.Group>

            <Form.Group controlId="userLastName" className='mb-3'>
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type='text'
                    placeholder='Last Name'
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    />
            </Form.Group>

            <Form.Group controlId="userEmail" className='mb-3'>
                <Form.Label>Email</Form.Label>
                <Form.Control 
                    type='email'
                    placeholder='Email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    />
            </Form.Group>
            
            <Form.Group controlId="userMobileNumber" className='mb-3'>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type='text'
                    placeholder='Mobile Number'
                    value={mobileNo}
                    onChange={(e) => setMobileNumber(e.target.value)}
                    />
            </Form.Group>

            <Form.Group controlId="userPassword" className='mb-3'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Password'
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    required
                    />
            </Form.Group>

            <Form.Group controlId="userPassword2" className='mb-3'>
                <Form.Label>Re-type Password</Form.Label>
                <Form.Control 
                    type='password'
                    placeholder='Password'
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    required
                    />
            </Form.Group>
            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" >
                    Register
                </Button>
            :
                <Button variant="muted" type="submit" id="submitBtn" disabled>
                    Register
                </Button>
            }
        </Form>
    </Card>
    </Container>
    </motion.div>
    )
}
