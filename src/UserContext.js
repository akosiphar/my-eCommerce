import React, { createContext, useEffect, useState } from 'react'

export const UserContext = createContext();

export default function UserProvider({ children }) {
    
    const [user, setUser] = useState({
        email: localStorage.getItem('email'),
        id: localStorage.getItem('_id'),
        access: localStorage.getItem('token'),
        isAdmin: JSON.parse(localStorage.getItem('isAdmin')),
    })

    

    function unSetUser () {
        localStorage.clear()
        sessionStorage.clear()
    }
    

    const value = {
        user,
        setUser,
        unSetUser,
    }



  return (
    <UserContext.Provider value={value}>
        {children}
    </UserContext.Provider>
  )
}
