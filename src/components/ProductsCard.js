import { useContext, useEffect, useState } from 'react'
import { Button, Card, Container, Form, Image, Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import ProductView from './ProductView';
import Swal from 'sweetalert2'
import { UserContext } from '../UserContext';

export default function ProductsCard({productProp}) {
    const {_id, name, description, price, stocks, foodType, productImage} = productProp
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true)
    const [quantity, setQuantity] = useState(0)

    // const [imageData, setImageData] = useState(null);

    // const imageURL;

/*     useEffect(() => {
        fetch('http://localhost:4000/products/allActiveProducts',{
            headers: {
                Authorization : `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((response) => response.json())
                .then((blob) => {
                    const file = event.target.files[0];
                    setImageData(blob.image)
                });
    }) */

    function handleAddToCart(product) {
        addToCart(product);
      }



    const addToCart = (e) => {
        e.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/addToCart/${_id}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: +quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(data) {
                function wait() {
                    Swal.fire({
                        title: "Added To Cart",
                        icon: "success"
                    })
                }
                wait()
                fetchData()
            } /* else {
                Swal.fire({
                    title: "",
                    icon: "error"
                })
            } */
        })
    }

    /* const randomColor = () => {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);
        return `rgb(${r}, ${g}, ${b})`;
    };
    
    const getContrastColor = (color) => {
        // convert color to RGB values
        const rgb = color.substring(4, color.length - 1)
            .replace(/ /g, '')
            .split(',');
        const r = parseInt(rgb[0]);
        const g = parseInt(rgb[1]);
        const b = parseInt(rgb[2]);
    
        // calculate color brightness
        const brightness = (r * 299 + g * 587 + b * 114) / 1000;
    
        // return black or white based on brightness
        return brightness > 128 ? 'black' : 'white';
    }; */

    /* Try: To refresh the component once the form submits */
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
            .then(res => res.json())
            .then(data => data)
    }

        useEffect(() => {
            fetchData()
        }, [])

        


  return (
    <>
    <Card className="mb-3" style={{width: "16rem"}}>
        <Card.Body className='p-0'>
            <Card.Header>{name}</Card.Header>
            <Card.Body>
                {/* <Image src={image} alt="image from database"/> */}
                <Image src={productImage} fluid className='mb-3' />
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle className='my-0'>Variant</Card.Subtitle>
                <Card.Text>{foodType}</Card.Text>
                <Button type="button" onClick={handleShow}>Add to cart</Button>
            </Card.Body>
        </Card.Body>
    </Card>

    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton className='bg-dark'>
            <Modal.Title><p className='h3 text-white'>{name}</p></Modal.Title>
        </Modal.Header>
            <Modal.Body>
                <Form onSubmit={addToCart}>
                    <Card>
                        <Container fluid className='p-3'>
                        <Card.Title className='my-0'>Description</Card.Title>
                        <Card.Text>{description}</Card.Text>
                        <Card.Title className='my-0'>Price</Card.Title>
                        <Card.Text>{price}</Card.Text>
                        <Card.Title className='my-0'>Remaining Stocks</Card.Title>
                        <Card.Text>{stocks}</Card.Text>
                        <Form.Control
                            className='mb-3'
                            placeholder="Enter Quantity"
                            type='number'
                            min="0"
                            inputMode='numeric'
                            value = {quantity}
                            onChange={e => setQuantity(e.target.value) }
                            required
                            />
                        <Button type="submit" onClick={handleClose}>Add to cart</Button>
                        </Container>
                    </Card>
                </Form>
            </Modal.Body>
        </Modal>
    </>
    )
}
