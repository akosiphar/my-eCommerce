import { useContext, useEffect, useState } from 'react'
import { Card } from 'react-bootstrap'
import { useNavigate, useParams } from 'react-router-dom'
import { UserContext } from '../UserContext'

export default function ProductView() {
    const { user } = useContext(UserContext)

  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')
  const [stocks, setStocks] = useState('')

  const {productId} = useParams()

  const navigate = useNavigate();

  useEffect(() => {
    console.log(productId);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
      setStocks(data.stocks);
    })
  }, [productId])
  return (
    <>
    <Card>
    <Card.Body className="text-center">
      <Card.Title>{name}</Card.Title>
      <Card.Subtitle>Description:</Card.Subtitle>
      <Card.Text>{description}</Card.Text>
      <Card.Subtitle>Price:</Card.Subtitle>
      <Card.Text>PhP {price}</Card.Text>
      <Card.Subtitle>Remaining Stocks</Card.Subtitle>
      <Card.Text>{stocks}</Card.Text>
    </Card.Body>
    </Card>
    </>
  )
}
