import React, { Fragment, useContext, useEffect, useState } from 'react'
import { Badge, Container, Nav, Navbar } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import { UserContext } from '../UserContext'

export default function AppNavbar() {
    const {user, setUser} = useContext(UserContext)

  return (
    <>
    <Navbar collapseOnSelect bg='dark' variant='dark' expand='lg' className='p-3'>
        <Container fluid>
            <Navbar.Brand className='me-auto' as={NavLink} to="/">SARI2</Navbar.Brand>
            <Navbar.Toggle aria-controls='responsive-navbar-nav' />
            <Navbar.Collapse id='responsive-navbar-nav'>
                <Container fluid className='d-flex flex-column flex-lg-row justify-content-end'>
                    <Nav>
                {user.email !== null
                ?
                    <Fragment>
                        <Nav.Link as={NavLink} to='/' href='#home'>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products" href='#products'>Products</Nav.Link>
                    {user.isAdmin !== false
                    ?
                        <Fragment>
                        <Nav.Link as={NavLink} to="/allOrders" href='#allOrders'>All Orders</Nav.Link>
                        <Nav.Link as={NavLink} to="/admin" href='#admin'>Dashboard</Nav.Link>
                        </Fragment>
                    :
                        <>
                        <Nav.Link as={NavLink} to="/userProfile" href="#userProfile">My Profile</Nav.Link>
                        <Nav.Link as={NavLink} to="/myOrders" href='myOrders'>My Orders</Nav.Link>
                        </>
                    }
                        <Nav.Link as={NavLink} to="/logout">Logout {user.email}</Nav.Link>

                    </Fragment>
                :
                    <Fragment>
                        <Nav.Link as={NavLink} to="/register" href='#register'>Register</Nav.Link>
                        <Nav.Link as={NavLink} to='/login' href='#login'>Login</Nav.Link>
                    </Fragment>
                }
                    </Nav>
                </Container>
            </Navbar.Collapse>
        </Container>
    </Navbar>
    </>
  )
}
